import React, { useState, useEffect } from 'react'
import Dashboard from './components/Dashboard';
import Sidebar from './components/Sidebar';
import Modal from './components/Modal'
import Settings from './components/Settings';
import Skypack from './components/Skypack'

const App = () => {

  const [active, setActive] = useState(false)
  const [activeSkypack, setActiveSkypack] = useState(false)
  const [download, setDownload] = useState(false)

  const [editorOptions, setEditorOptions] = useState({
    fontSize: 14,
    minimap: {
      enabled: true
    },
    lineNumbers: 'on',
    theme:  'vs-dark',
  })

  const handleOnOptionsChanged = options => {
    console.log(options, 85588)
    setEditorOptions(options)
  } 

  const toggle = () => {
    setActive(!active)
  }

  const toggleSkypack = () => {
    setActiveSkypack(!activeSkypack)
  }

  const handleDownload = () => {
    setDownload(true)
  }

  return (
    <form className="app">
        <Sidebar toggle={toggle} toggleSkypack={toggleSkypack} download={handleDownload} />
        
        <Modal active={active} toggle={toggle}>
          <Settings  editorOptions={editorOptions} handleOnOptionsChanged={handleOnOptionsChanged} />
        </Modal>
         
        <Modal active={activeSkypack} toggle={toggleSkypack}>
          <Skypack />
        </Modal>

        <Dashboard editorOptions={editorOptions} download={download} setDownload={setDownload} />
    </form>
  );
};

export default App;
